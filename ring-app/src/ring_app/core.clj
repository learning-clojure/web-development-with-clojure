(ns ring-app.core
  (:require [ring.adapter.jetty :as jetty]
            [compojure.core :as compojure]
            [ring.util.http-response :as response]
            [ring.middleware.reload :refer [wrap-reload]]
            [ring.middleware.format :refer [wrap-restful-format]]))

(defn response-handler [request]
  (response/ok
    (str "<html><body> your IP is: "
         (:remote-addr request)
         "</body></html>")))

(compojure/defroutes handler
                     (compojure/GET "/" request response-handler)
                     (compojure/GET "/foo" request (interpose ", " (keys request)))
                     (compojure/GET "/:id" [id] (str "<p>the id is: " id "</p>"))
                     (compojure/POST "/json" [id] (response/ok {:result id})))

(defn wrap-nocache [handler]
  (fn [request]
    (-> request
        handler
        (assoc-in [:headers "Pragma"] "no-cache"))))

(defn wrap-formats [handler]
  (wrap-restful-format
    handler
    {:formats [:json-kw :transit-json :transit-msgpack]}))

(defn -main []
  (jetty/run-jetty
    (-> #'handler wrap-nocache wrap-reload wrap-formats)
    {:port  3000
     :join? false}))

;; test command
;; curl -H "Content-Type: application/json" -X POST -d '{"id":1}' localhost:3000/json ; echo
;; {"result":1}
;;
;; curl http://localhost:3000/bar ; echo
;; <p>the id is: bar<</p>
;; curl http://localhost:3000/foo ; echo
;; ["ssl-client-cert",", ","protocol",", ","remote-addr",", ","params",", ","route-params",", ","headers",", ",
;; "server-port",", ","content-length",", ","compojure/route",", ","content-type",", ","character-encoding",", ",
;; "uri",", ","server-name",", ","query-string",", ","body",", ","scheme",", ","request-method"]
