# myapp

Sample `Hello World` Clojure application.

## Usage

To run application execute following command:

```
lein run Obligatory
```

This will output: `Obligatory Hello, World!`

## License

Copyright © 2017 Eduard Luhtonen

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
