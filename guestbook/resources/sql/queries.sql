-- :name save-message! :! :n
-- :doc creates a new user record
INSERT INTO guestbook
(name, message, posted_at)
VALUES (:name, :message, :posted_at)

-- :name get-messages :? :*
-- :doc selects all available messages
SELECT * FROM guestbook
