# guestbook

Sample Guestbook web application using Luminus and Clojure.

## Prerequisites

You will need [Leiningen][1] 2.0 or above installed.

[1]: https://github.com/technomancy/leiningen

## Running

To start a web server for the application, run:

    lein run

## Testing in nREPL

When the application starts in development mode, it automatically runs the nREPL on port `7000`.

To connect from command line use following command: `lein repl :connect 7000`.

Try running the queries in nREPL:
```
;;switch to the namespace
(use 'guestbook.db.core)
;;=> nil

;; start database connection
(mount/start
  #'guestbook.config/env
  #'guestbook.db.core/*db*)
;;=> {:started ["#'guestbook.db.core/*db*"]}

;;check if we have any existing data
(get-messages)
;;=> ()

;;create a test message
(save-message! {:name "Bob"
                :message "Hello World"
                :posted_at (java.util.Date.)})

;;=> 1

;;check that the message is saved correctly
(get-messages)
;;=> ({:id 1, :name "Bob", :message "Hello World", :posted_at #inst"2017-02-04T23:13:38.654000000-00:00"})

```

## Building and running as standalone application

Application can be packaged into a runnable jar as follows:
```
lein uberjar
```

Database connection should be specified as an environment variable. 
Create a connection variable and then run our application as follows:
```
DATABASE_URL="jdbc:h2:./guestbook_dev.db" java -jar target/guestbook.jar
```

## License

Copyright © 2017 Eduard Luhtonen
